
import { fetchUsers, fetchUsersChats, fetchchatList, addchatList, fetchChat, createUser } from "./requests";
import Helper from "../helper";

export const fetchChatAPI = async (params, user) => {
  let chat = await fetchChat(params);
  if (chat.data) {
    return Helper.chatComponentData(chat, user);
  }
  return false;
}

export const fetchUserToUserChatsAPI = async (params, user) => {
  let chat = await fetchUsersChats(params);
  if (chat.data) {
    let data = await Helper.chatComponentData(chat, user);
    return data;
  }
  return false;

}


export const fetchUsersAPI = async (params, user) => {
  let getUsers = await fetchUsers(params);
  if (getUsers.data) {
    getUsers.data.map((item) => item.isAdded = false);
    return getUsers;
  }
  return false;
}




