import axios from 'axios';

const API_ROOT = process.env.REACT_APP_SERVER_API_ENDPOINT

axios.defaults.baseURL = API_ROOT;

export const createUser = (data) => {
    return axios({
        method: 'post',
        url: '/users',
        data
    })
        .then(res => res.data)
}
// Fetch User
export const fetchUsers = (params = '') => {
    return axios.get('/users' + params)
        .then(res => res.data)
}
// Fetch Users for ChatList
export const fetchChatListUsers = (params = '') => {
    return axios.get('/chatList/users' + params)
        .then(res => res.data)
}

export const fetchUsersChats = (query) => {
    return axios.get('/chatList/user-user-chat' + query)
        .then(res => res.data)
}

export const addchatList = (data) => {
    return axios({
        method: 'post',
        url: '/chatList/add',
        headers: {
            'Content-Type': 'application/json'
        },
        data
    })
        .then(res => res.data)
}

export const fetchchatList = (data) => {
    return axios.get('/chatList' + data)
        .then(res => res.data)
}

export const fetchChat = (query) => {
    return axios.get('/chatList/chat' + query)
        .then(res => res.data)
}

export const updateReceivers = (data) => {
    return axios({
        method: 'post',
        url: '/chatList/update',
        headers: {
            'Content-Type': 'application/json'
        },
        data
    })
        .then(res => res.data)
}