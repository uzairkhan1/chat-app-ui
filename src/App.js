import React, { Component } from "react";
import NavBar from "./components/NavBar";
import Modal from "react-bootstrap/lib/Modal";
import FormControl from "react-bootstrap/lib/FormControl";
import InputGroup from "react-bootstrap/lib/InputGroup";
import Button from "react-bootstrap/lib/Button";
import FormGroup from "react-bootstrap/lib/FormGroup";
import 'font-awesome/css/font-awesome.min.css';
import List from "./components/List";
import ErrorModal from "./components/ErrorModal";
import LoadingModal from "./components/LoadingModal";
import ScreenOptions from "./components/ScreenOptions";
import UserToUser from "./components/UserToUser";
import UserToMultiUser from "./components/UserToMultiUser";
import "react-chat-elements/dist/main.css";
import "./index.css";
import io from "socket.io-client";
import { fetchchatList, addchatList, createUser } from "./services/requests";
import { fetchChatAPI, fetchUsersAPI, fetchUserToUserChatsAPI } from "./services/API";
import {
  NotificationContainer,
  NotificationManager
} from "react-notifications";
import "react-notifications/lib/notifications.css";
import axios from "axios";
import Helper from "./helper";
import { FetchImage } from 'random-image-unsplash';

const SOCKET_URI = process.env.REACT_APP_SERVER_URI

class App extends Component {
  socket = null;

  state = {
    signInModalShow: false,
    users: [],
    user: null,
    userName: '',
    error: false,
    errorMessage: "",
    selectedChat: {},
    screen: '',
    chat: [],
    chatList: [],
    selectedMultiUserId: [],
    imageConfig: { type: 'user', width: 400, height: 400 },
    chatWithPagination: {},
    usersWithPagination: { data: [] },
    selectedChatId: ''

  };

  /**
   *
   * Setups Axios to monitor XHR errors.
   * Initiates and listen to socket.
   * fetches User's list from backend to populate.
   */

  componentDidMount() {
    this.initAxios();
    this.initSocketConnection();
    this.fetchUsers('', true);
    this.setupSocketListeners();
    this.setState({ signInModalShow: true });

  }

  initSocketConnection() {
    this.socket = io.connect(SOCKET_URI);
  }

  // Checks if request from axios fails

  initAxios() {
    axios.interceptors.request.use(
      config => {
        this.setState({ loading: true });
        return config;
      },
      error => {
        this.setState({ loading: false });
        this.setState({
          errorMessage: `Couldn't connect to server. try refreshing the page.`,
          error: true
        });
        return Promise.reject(error);
      }
    );
    axios.interceptors.response.use(
      response => {
        this.setState({ loading: false });
        return response;
      },
      error => {
        this.setState({ loading: false });
        this.setState({
          errorMessage: `Some error occured. try after sometime`,
          error: true
        });
        return Promise.reject(error);
      }
    );
  }

  //Shows error if client gets disconnected.
  onClientDisconnected() {
    NotificationManager.error(
      "Connection Lost from server please check your connection.",
      "Error!"
    );
  }

  //Established new connection if reconnected.
  onReconnection = () => {
    if (this.state.user) {
      this.socket.emit("sign-in", this.state.user);
      NotificationManager.success("Connection Established.", "Reconnected!");
    }
  }

  // Setup all listeners
  setupSocketListeners() {
    this.socket.on("message", this.onMessageRecieved);
    this.socket.on("multi-user-message", this.onMultiUserMessageRecieved);
    this.socket.on("group-message", this.onMultiUserMessageRecieved);
    this.socket.on("reconnect", this.onReconnection);
    this.socket.on("disconnect", this.onClientDisconnected.bind(this));
  }


  onMessageRecieved = (event) => {
    let chat = {
      position: 'left',
      type: 'text',
      text: event.text,
      date: new Date(),
      title: event.sender_name

    }
    if (this.state.selectedChat._id === event.sender) {
      this.addUserChat(chat);
    }
    else NotificationManager.info(event.text, event.sender_name);
  }

  onMultiUserMessageRecieved = (event) => {
    let chat = {
      position: 'left',
      type: 'text',
      text: event.text,
      date: new Date(),
      title: event.sender_name
    }
    if (this.state.selectedChat._id == event.chat_list_id) this.addUserChat(chat);
    else NotificationManager.info(event.sender_name + ': ' + event.text, event.list_title, );
  }

  fetchUsers = async (params, clearUser) => {
    let usersAPI = await fetchUsersAPI(params);
    if (usersAPI.data) {
      let users = clearUser ? usersAPI.data : [...this.state.usersWithPagination.data, ...usersAPI.data];
      usersAPI.data = users;
      this.setState({ usersWithPagination: usersAPI });
    }
  }

  updatedchatList = async (params, clearUser) => {
    let chatListAPI = await fetchchatList(params);
    if (chatListAPI.data) {
      let list = clearUser ? chatListAPI.data : [...this.state.chatList.data, ...chatListAPI.data];
      chatListAPI.data = list;
      this.setState({ chatList: chatListAPI });
    }
  }


  onUserClickedForLogin = (user) => {
    this.setState({ user, signInModalShow: false });
    this.loginUser(user);
  }

  loginUser = (user) => {
    this.socket.emit("sign-in", user);
  }

  onUserChatClicked = async (_user) => {
    let params = '?sender=' + this.state.user._id + '&receiver=' + _user._id;
    let chat = [];
    let getChat = await fetchUserToUserChatsAPI(params, this.state.user);
    if (getChat.chat) chat = getChat.chat;
    this.setState({ selectedMultiUserId: [_user._id], chat, selectedChat: _user, chatWithPagination: getChat.chatWithPagination });
  }

  onUserChatListClicked = (list) => {
    let params = '?chat_list_id=' + list._id;
    let selectedChat = Helper.getUserFromReceivers(list.receivers, this.state.user, list._id);
    this.setState({ selectedChat, chat: [] })
    this.getUpdatedChat(params)
  }


  // creates message in a format in which messageList can render.
  createMessage = (text) => {
    let message = {
      text: text,
      sender: this.state.user._id,
      receiver: this.state.selectedChat._id,// receiver id
      sender_name: this.state.user.name,
    };
    let chat = {
      position: 'right',
      type: 'text',
      text: text,
      date: new Date(),
    }
    this.addUserChat(chat);
    this.socket.emit("message", message);
  }

  // Multi User - section - start

  createchatList = async (chatListName) => {
    let screen = this.state.screen;
    let loginUserId = this.state.user._id;
    let data = {
      created_by: loginUserId,
      receivers: this.state.selectedMultiUserId,
      chat_type: this.state.screen,
    }
    FetchImage(this.state.imageConfig).then((image) => console.log("image ", image))
    if (screen === 'user-group') {
      let imageConfig = this.state.imageConfig;
      imageConfig.type = 'perspective';
      let image = await FetchImage(imageConfig);
      data.name = chatListName;
      data.image = image;
    }
    addchatList(data).then(res => {
      if (res.data) {
        let chatList = this.state.chatList;
        chatList.data = [...chatList.data, ...res.data];
        let title = this.state.screen === 'user-group' ? 'Group' : 'Chat List'
        this.setState({ chatList, selectedChat: res.data });
        NotificationManager.success('Successfully created ' + title + '!', title);
      }

    });
    this.clearSelection();
  }

  multiUserOnUserClicked = (e) => {
    let selectedMultiUserId = this.state.selectedMultiUserId;
    let usersWithPagination = this.state.usersWithPagination;
    if (selectedMultiUserId.includes(e._id)) {
      selectedMultiUserId.map((item) => item !== e._id);
    }
    else {
      selectedMultiUserId.push(e._id)
    }
    usersWithPagination.data.map((item) => {
      if (item._id === e._id) item.isAdded = !item.isAdded;
    });

    this.setState({ selectedMultiUserId, usersWithPagination });
  }

  multiUserChatClicked = (e) => {
    let parms = '?user_id=' + this.state.user._id + '&chat_list_id=' + e._id;
    this.setState({ selectedChat: e });
    this.getUpdatedChat(parms, true);
  }

  getUpdatedChat = async (parms, clearSelection) => {
    let getChat = await fetchChatAPI(parms, this.state.user);
    if (getChat.chatWithPagination) {
      this.setState({ chat: clearSelection ? getChat.chat : [...getChat.chat, ...this.state.chat], chatWithPagination: getChat.chatWithPagination });
    }
  }

  getUserToUserUpdatedChat = async (parms) => {
    let getChat = await fetchUserToUserChatsAPI(parms, this.state.user);
    if (getChat.chatWithPagination) {
      this.setState({ chat: [...getChat.chat, ...this.state.chat], chatWithPagination: getChat.chatWithPagination });
    }
  }

  clearSelection = () => {
    let usersWithPagination = this.state.usersWithPagination;
    if (usersWithPagination.data.length > 0) {
      usersWithPagination.data.map((item) => {
        item.isAdded = false
        return item;
      })

    }

    this.setState({ selectedMultiUserId: [], usersWithPagination })
  }


  // creates message in a format in which messageList can render.
  multiUserCreateMessage = (text) => {
    let selectedChat = this.state.selectedChat;
    let loginUser = this.state.user;
    let channelName = selectedChat.chat_type == 'user-multi-user' ? 'multi-user-message' : 'group-message'
    let message = {
      text: text,
      sender: loginUser._id,
      chat_list_id: selectedChat._id,
      sender_name: this.state.user.name,
      list_title: selectedChat.chat_type == 'user-multi-user' ? Helper.notificationTitle(selectedChat.receivers, loginUser) : selectedChat.name
    };
    let chat = {
      position: 'right',
      type: 'text',
      text: text,
      date: new Date()
    }
    this.addUserChat(chat);
    this.socket.emit(channelName, message);
  }
  // Multi User - section - end

  // change screen
  toggleViews = () => {
    this.setState({
      screen: '',
      selectedChat: {},
      selectedMultiUserId: []
    });
  }

  // User login form -start
  onUserNameKeyPress = (e) => {
    if (e.key === "Enter") {
      this.submitLogin();
    }
  }

  submitLogin = async () => {
    let image = await FetchImage(this.state.imageConfig);
    let data = {
      name: this.state.userName,
      image
    }
    createUser(data).then(res => {
      if (res.data) {
        let user = res.data;
        this.setState({ user, signInModalShow: false });
        this.loginUser(user);
      }
    }
    );
  }
  // User login form - end

  changeScreen = async (screen) => {
    let loginUserId = this.state.user._id;
    if (screen !== 'user-user') {
      let params = '?chat_type=' + screen + '&user_id=' + loginUserId;
      this.updatedchatList(params, true);

    }
    this.fetchUsers('?user_id=' + loginUserId, true);
    this.setState({
      screen,
      selectedChat: {},
      selectedMultiUserId: []
    });
  }

  addUserChat = (_chat) => {
    let chat = this.state.chat;
    chat.push(_chat);
    this.setState({ chat });
  };


  renderScreen = () => {
    let loginUserId = '';
    switch (this.state.screen) {
      case 'user-user':
        loginUserId = this.state.user._id;
        return <UserToUser
          user={this.state.user}
          usersWithPagination={this.state.usersWithPagination}
          chatWithPagination={this.state.chatWithPagination}
          onUserChatClicked={this.onUserChatClicked}
          onChatClicked={this.onUserChatListClicked}
          signedInUser={this.state.user}
          onSendClicked={this.createMessage}
          onBackPressed={this.toggleViews}
          selectedChat={this.state.selectedChat}
          addUserChat={this.addUserChat}
          chat={this.state.chat}
          chatList={this.state.chatList}
          onScrollUserList={(params) => this.fetchUsers(params + '&user_id=' + loginUserId)}
          getUserToUserUpdatedChat={this.getUserToUserUpdatedChat}

        />
      case 'user-multi-user':
        loginUserId = this.state.user._id;
        return <UserToMultiUser
          user={this.state.user}
          chatList={this.state.chatList}
          multiUserOnUserClicked={(e) => this.multiUserOnUserClicked(e)}
          onSendClicked={(e) => this.multiUserCreateMessage(e)}
          onBackPressed={this.toggleViews}
          selectedChat={this.state.selectedChat}
          addUserChat={this.addUserChat}
          chat={this.state.chat}
          selectedMultiUserId={this.state.selectedMultiUserId}
          createchatList={this.createchatList}
          multiUserChatClicked={(e) => this.multiUserChatClicked(e)}
          clearSelection={this.clearSelection}
          screen={this.state.screen}
          chatWithPagination={this.state.chatWithPagination}
          usersWithPagination={this.state.usersWithPagination}
          getUpdatedChat={this.getUpdatedChat}
          onScrollUserList={(params) => this.fetchUsers(params + '&user_id=' + loginUserId)}
          onScrollChatList={(params) => this.updatedchatList(params, false)}
        />
      case 'user-group':
        loginUserId = this.state.user._id;
        return <UserToMultiUser
          user={this.state.user}
          chatList={this.state.chatList}
          multiUserOnUserClicked={this.multiUserOnUserClicked}
          onSendClicked={this.multiUserCreateMessage}
          onBackPressed={this.toggleViews}
          selectedChat={this.state.selectedChat}
          addUserChat={this.addUserChat}
          chat={this.state.chat}
          selectedMultiUserId={this.state.selectedMultiUserId}
          createchatList={this.createchatList}
          multiUserChatClicked={(e) => this.multiUserChatClicked(e)}
          clearSelection={this.clearSelection}
          screen={this.state.screen}
          chatWithPagination={this.state.chatWithPagination}
          usersWithPagination={this.state.usersWithPagination}
          getUpdatedChat={this.getUpdatedChat}
          onScrollUserList={(params) => this.fetchUsers(params + '&user_id=' + loginUserId)}
          onScrollChatList={(params) => this.updatedchatList(params, false)}

        />
      default:
        return <ScreenOptions changeScreen={this.changeScreen} />;

    }

  }


  render() {


    return (
      <div>
        <NavBar signedInUser={this.state.user} changeScreen={this.changeScreen} />
        {this.renderScreen()}
        <Modal show={this.state.signInModalShow}>
          <Modal.Header>
            <Modal.Title>Sign In as:</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <div>Register New User</div>
            <FormGroup>
              <InputGroup>
                <FormControl
                  type="text"
                  value={this.state.userName}
                  onChange={(e) => this.setState({ userName: e.target.value })}
                  onKeyPress={this.onUserNameKeyPress}
                  placeholder="Enter UserName"
                />
                <InputGroup.Button>
                  <Button
                    disabled={!this.state.userName}
                    onClick={this.submitLogin}
                  >
                    Send
                  </Button>
                </InputGroup.Button>
              </InputGroup>
            </FormGroup>

            {
              this.state.users.length > 0 &&
              <div><hr /><div>OR Select User</div></div>
            }

            <List
              listData={this.state.usersWithPagination}
              onClick={this.onUserClickedForLogin}
              onScroll={this.fetchUsers}
              notFoundText={'No users to show.'}
            />

          </Modal.Body>
        </Modal>
        <ErrorModal
          show={this.state.error}
          errorMessage={this.state.errorMessage}
        />
        <LoadingModal show={this.state.loading} />
        <NotificationContainer />
      </div>
    );
  }
}

export default App;
