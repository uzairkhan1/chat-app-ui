

const Helper = {
  capitalize: (str) => {
    if (typeof str !== 'string') return ''
    return str.charAt(0).toUpperCase() + str.slice(1)
  },

  getUserNames: (_title, _receivers, user) => {
    if (_title) return _title;
    let _currentUserName = user.name;
    let _names = '';
    if (_receivers && _receivers.length) {
      _receivers.map((item, index) => {
        if (item.name !== _currentUserName) _names += item.name + ', ';
      })
      _names = _names.substring(0, _names.length - 2);
    }
    return _names;
  },

  titleText: (str, length, ending) => {
    if (length == null) {
      length = 100;
    }
    if (ending == null) {
      ending = '...';
    }
    if (str && str.length > length) {
      return str.substring(0, length - ending.length) + ending;
    } else {
      return str;
    }
  },

  notificationTitle: (receivers, loginUser) => {
    let names = Helper.getUserNames('', receivers, loginUser);
    return Helper.titleText(names, 40);
  },

  isAdmin: (admin, loginUser) => {
    if (admin._id === loginUser._id) return true;
    else return false;
  },

  getUserFromReceivers: (receivers, user, chat_list_id) => {
    let chatList = user._id === receivers[0]._id ? receivers[1] : receivers[0];
    chatList.chat_list_id = chat_list_id;
    return chatList;
  },

  chatComponentData: (chat, user) => {
    chat.data.reverse();
    let getChat = [];
    let chatWithPagination = {};
    chat.data.map((item) => {
      getChat.push({
        position: user._id === item.sender._id ? 'right' : 'left',
        type: 'text',
        text: item.text,
        date: new Date(item.createdAt),
        title: user._id !== item.sender._id ? Helper.capitalize(item.sender.name) : ''
      });

      chatWithPagination = chat;
    });
    return { chat: getChat, chatWithPagination }
  },
  getChatListName: (selectedChat, user) => {
    if (selectedChat.name) return Helper.titleText(selectedChat.name, 80)
    else return Helper.titleText(Helper.getUserNames(selectedChat.name, selectedChat.receivers, user, 80), 80)
  }


}

export default Helper;