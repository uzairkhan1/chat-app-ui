import React from "react";
import Button from "react-bootstrap/lib/Button";
import "../style/ScreenOptions.css";

/**
 *
 * Renders top navbar and shows the current signed in user.
 */
const ScreenOptions = (props) => {
  return (
    <div>
      <Button variant="secondary" className="center-block main-scren-btn" onClick={() => props.changeScreen('user-user')}>
        User to User Chat
        </Button>
      <Button variant="secondary" className="center-block  main-scren-btn" onClick={() => props.changeScreen('user-multi-user')}>
        User to Multi User Chat
        </Button>

      <Button variant="secondary" className="center-block  main-scren-btn" onClick={() => props.changeScreen('user-group')}>
        User to Group Chat
        </Button>
    </div >
  );
}
export default ScreenOptions;