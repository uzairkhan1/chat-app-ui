import React, { Component } from "react";
import FormControl from "react-bootstrap/lib/FormControl";
import InputGroup from "react-bootstrap/lib/InputGroup";
import Button from "react-bootstrap/lib/Button";
import FormGroup from "react-bootstrap/lib/FormGroup";
import Col from "react-bootstrap/lib/Col";
import Jumbotron from "react-bootstrap/lib/Jumbotron";
import Glyphicon from "react-bootstrap/lib/Glyphicon";
import {
  MessageList,
  Navbar as NavbarComponent,
  Avatar
} from "react-chat-elements";

/**
 *
 * ChatBox Component
 *
 * displays all the messages from chat history.
 * renders message text box for input.
 */

export default class ChatBox extends Component {
  state = {
    messageText: "",
    chat: [],
    selectedChat: {}
  };
  static getDerivedStateFromProps(props, state) {
    return {
      chat: props.chat,
      selectedChat: props.selectedChat
    }
  }

  /**
   *
   * Sends a message only if it is not falsy.
   */
  onSendClicked = () => {
    if (!this.state.messageText) {
      return;
    }
    this.props.onSendClicked(this.state.messageText);
    this.setState({ messageText: "" });
  }
  onMessageInputChange = (e) => {
    this.setState({ messageText: e.target.value });
  }
  /**
   *
   * @param {KeyboardEvent} e
   *
   * listen for enter pressed and sends the message.
   */
  onMessageKeyPress = (e) => {
    if (e.key === "Enter") {
      this.onSendClicked();
    }
  }

  chatMsgListScroll = (e) => {

    let props = this.props;
    let hasNextPage = props.chatWithPagination.hasNextPage || false;
    let nextPage = props.chatWithPagination.nextPage || 0;
    let chat_list_id = props.selectedChat._id || 0;
    // load more
    if (e.target.scrollTop === 0 && hasNextPage) {
      let sender = props.chatWithPagination.sender;
      let receiver = props.chatWithPagination.receiver;
      let parms = '?page=' + nextPage + '&sender=' + sender + '&receiver=' + receiver;
      props.getUserToUserUpdatedChat(parms);
    }
  }

  render() {
    return (
      <div>
        {this.state.selectedChat._id ? (
          <div>
            <NavbarComponent
              left={
                <div>
                  <Col mdHidden lgHidden>
                    <p className="navBarText">
                      <Glyphicon
                        onClick={this.props.onBackPressed}
                        glyph="chevron-left"
                      />
                    </p>
                  </Col>
                  <Avatar
                    src={this.props.selectedChat.image}
                    alt={"logo"}
                    size="large"
                    type="circle flexible"
                  />
                  <p className="navBarText">{this.props.selectedChat.name}</p>
                </div>
              }
            />
            <MessageList
              className="message-list"
              lockable={true}
              toBottomHeight={"100%"}
              dataSource={this.state.chat}
              onScroll={(e) => this.chatMsgListScroll(e)}
            />
            <FormGroup>
              <InputGroup>
                <FormControl
                  type="text"
                  value={this.state.messageText}
                  onChange={this.onMessageInputChange}
                  onKeyPress={this.onMessageKeyPress}
                  placeholder="Type a message here (Limit 3000 characters)..."
                  className="messageTextBox"
                  maxLength="3000"
                  autoFocus
                />
                <InputGroup.Button>
                  <Button
                    disabled={!this.state.messageText}
                    className="sendButton"
                    onClick={this.onSendClicked}
                  >
                    Send
                  </Button>
                </InputGroup.Button>
              </InputGroup>
            </FormGroup>
          </div>
        ) : (
            <div>
              <Jumbotron>
                <h1>Hello, {(this.props.signedInUser || {}).name}!</h1>
                <p>Select a friend to start a chat.</p>
              </Jumbotron>
            </div>
          )}
      </div>
    );
  }
}
