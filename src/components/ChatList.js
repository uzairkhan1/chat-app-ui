import React, { useState, useEffect } from "react";
import BottomScrollListener from 'react-bottom-scroll-listener';
import UserListItem from "./UserListItem";
import Helper from "../helper";


const ChatList = (props) => {
  const [listData, setListData] = useState([]);

  useEffect(() => {
    setListData(props.listData);
  }, [props.listData]);

  const loadMore = () => {
    if (listData.hasNextPage) {
      let user_id = listData.user_id;
      let chat_type = listData.chat_type;
      props.onScroll('?page=' + listData.nextPage + '&user_id=' + user_id + '&chat_type=' + chat_type);
    }
  }


  return (
    <div>
      {listData.data && listData.data.length ?
        (<BottomScrollListener onBottom={() => loadMore()}>
          {scrollRef => (
            <div ref={scrollRef} className="userlist-container">
              {
                listData.data.map((item, index) => {
                  return (<div key={index}>
                    <UserListItem
                      index={index}
                      listItem={item}
                      name={item.name ? item.name : Helper.getChatListName(item, props.user)}
                      image={item.image}
                      onClick={() => props.onClick ? props.onClick(item) : {}}
                      hideImage={listData.chat_type == 'user-multi-user'}
                    />
                  </div>)
                })
              }
            </div>
          )}
        </BottomScrollListener>)
        : (
          <div className="text-center no-users">{props.notFoundText}</div>
        )}
    </div>
  );
}
export default ChatList;