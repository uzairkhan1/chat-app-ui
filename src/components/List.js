import React, { useState, useEffect } from "react";
import BottomScrollListener from 'react-bottom-scroll-listener';
import UserListItem from "./UserListItem";

const List = (props) => {
  const [listData, setListData] = useState({ data: [] });

  useEffect(() => {
    setListData(props.listData);

  }, [props.listData]);

  const loadMore = () => {
    if (listData.hasNextPage) {
      props.onScroll('?page=' + listData.nextPage);
    }
  }
  const showAdded = props.showAdded || false;

  return (
    <div>
      {listData.data && listData.data.length ?
        (<BottomScrollListener onBottom={() => loadMore()}>
          {scrollRef => (
            <div ref={scrollRef} className="userlist-container">
              {
                listData.data.map((item, index) => {
                  return (<div key={index}>
                    <UserListItem
                      index={index}
                      name={item.name}
                      image={item.image}
                      onClick={() => props.onClick ? props.onClick(item) : {}}
                      showAdded={showAdded}
                      isAdded={item.isAdded}
                      listItem={item}
                    />
                  </div>)
                })
              }
            </div>
          )}
        </BottomScrollListener>)
        : (
          <div className="text-center no-users">{props.notFoundText}</div>
        )}
    </div>
  );
}
export default List;