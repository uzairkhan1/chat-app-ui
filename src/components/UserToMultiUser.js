import React, { useState } from "react";
import Grid from "react-bootstrap/lib/Grid";
import Row from "react-bootstrap/lib/Row";
import Col from "react-bootstrap/lib/Col";
import FormGroup from "react-bootstrap/lib/FormGroup";
import FormControl from "react-bootstrap/lib/FormControl";
import InputGroup from "react-bootstrap/lib/InputGroup";
import Button from "react-bootstrap/lib/Button";
import List from "./List";
import ChatList from "./ChatList";


import ChatBoxMultiUser from "./ChatBoxMultiUser";

const UserToMultiUser = (props) => {
    const [name, setName] = useState('');
    const clearSelection = () => {
        props.clearSelection();
        setName('');
    }
    const submitForm = () => {
        props.createchatList(name);
        setName('');

    }

    const renderChatlistForm = () => {
        return (<div style={{ marginBottom: '10px' }}>
            <FormGroup style={{ border: 'none' }}>
                {
                    props.screen === 'user-group' &&

                    <InputGroup style={{ marginBottom: '5px', width: '230px' }}>
                        <FormControl
                            type="text"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            onKeyPress={(e) => {
                                if (e.key === "Enter") submitForm()
                            }}
                            placeholder="Enter Group Name"
                        />
                    </InputGroup>
                }
                <Button
                    onClick={() => submitForm()}
                    disabled={(props.screen === 'user-group' && name === '')}
                >
                    Add to {props.screen === 'user-group' ? 'Group' : 'Chat'}
                </Button>
                <Button
                    onClick={clearSelection}
                >
                    Clear Selection
  </Button>
            </FormGroup>


        </div>)
    }
    const title = props.screen === 'user-group' ? 'Groups' : 'ChatList';
    const isGroup = props.screen === 'user-group' ? false : true;
    console.log("sdf props", props);

    return (
        <Grid>
            <Row>
                <Col md={4}>

                    <h3> {title} </h3>
                    <ChatList
                        listData={props.chatList}
                        user={props.user}
                        onClick={props.multiUserChatClicked}
                        onScroll={props.onScrollChatList}
                        notFoundText={'No ' + title + ' to show.'}
                        hideImage={isGroup}
                    />
                    <hr />
                    <h3> All Users </h3>
                    {
                        props.selectedMultiUserId && props.selectedMultiUserId.length ? renderChatlistForm() : null
                    }
                    <List
                        listData={props.usersWithPagination}
                        onClick={props.multiUserOnUserClicked}
                        onScroll={props.onScrollUserList}
                        notFoundText={'No users to show.'}
                        showAdded={true}
                    />
                </Col>
                {
                    <Col md={8}>
                        <ChatBoxMultiUser
                            user={props.user}
                            onSendClicked={props.onSendClicked}
                            onBackPressed={props.onBackPressed}
                            selectedChat={props.selectedChat}
                            chat={props.chat}
                            chatWithPagination={props.chatWithPagination}
                            getUpdatedChat={props.getUpdatedChat}
                        />
                    </Col>
                }
            </Row>
        </Grid>
    );
}
export default UserToMultiUser;