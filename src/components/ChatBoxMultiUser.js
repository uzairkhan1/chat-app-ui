import React, { useState } from "react";
import FormControl from "react-bootstrap/lib/FormControl";
import InputGroup from "react-bootstrap/lib/InputGroup";
import Button from "react-bootstrap/lib/Button";
import FormGroup from "react-bootstrap/lib/FormGroup";
import Col from "react-bootstrap/lib/Col";
import Jumbotron from "react-bootstrap/lib/Jumbotron";
import Glyphicon from "react-bootstrap/lib/Glyphicon";
import {
  MessageList,
  Navbar as NavbarComponent,
} from "react-chat-elements";
import "../style/ChatboxMultiUser.css";
import AddMemberModal from "./AddMemberModal";
import Helper from "../helper";
import { fetchChatListUsers, fetchChat } from "../services/requests";


const ChatBoxMultiUser = (props) => {

  const [messageText, setMessageText] = useState('');
  const [showMemberModal, setShowMemberModal] = useState(false);
  const [chatListData, setChatListData] = useState({});

  const onSendClicked = () => {
    if (!messageText) return false;
    props.onSendClicked(messageText);
    setMessageText('');
  }
  const onMessageInputChange = (e) => {
    setMessageText(e.target.value);
  }
  const onMessageKeyPress = (e) => {
    if (e.key === "Enter") {
      onSendClicked();
    }
  }
  const _setShowMemberModal_ = (value) => {
    setShowMemberModal(value)
  }
  const openModal = async () => {
    let fetchListDat = await fetchChatListUsers('?chat_list_id=' + props.selectedChat._id, props.selectedChat.receivers);
    if (fetchListDat.data) {
      setChatListData(fetchListDat);
      setShowMemberModal(true);
    }
  }

  const isAdmin = () => {
    if (props.selectedChat && props.selectedChat._id) return Helper.isAdmin(props.selectedChat.created_by, props.user);
    return false;
  }

  const chatMsgListScroll = (e) => {
    let hasNextPage = props.chatWithPagination.hasNextPage || false;
    let nextPage = props.chatWithPagination.nextPage || 0;
    let chat_list_id = props.selectedChat._id || 0;
    // load more
    if (e.target.scrollTop === 0 && hasNextPage) {
      let parms = '?page=' + nextPage + '&chat_list_id=' + chat_list_id;
      props.getUpdatedChat(parms);
    }
  }

  return (
    <div>
      {props.selectedChat._id ? (
        <div>
          <NavbarComponent
            left={
              <div>
                <Col mdHidden lgHidden>
                  <p className="navBarText">
                    <Glyphicon
                      onClick={props.onBackPressed}
                      glyph="chevron-left"
                    />
                  </p>
                </Col>
                <p className="navBarText">{Helper.titleText(Helper.getUserNames(props.selectedChat.name, props.selectedChat.receivers, props.user, 80), 80)}</p>
                {
                  isAdmin() &&
                  <i className="fa fa-plus add-member-icon" onClick={() => openModal()} ></i>
                }
              </div>
            }
          />
          <MessageList
            className="message-list"
            lockable={true}
            toBottomHeight={"100%"}
            dataSource={props.chat}
            onScroll={(e) => chatMsgListScroll(e)}
          />
          <FormGroup>
            <InputGroup>
              <FormControl
                type="text"
                value={messageText}
                onChange={onMessageInputChange}
                onKeyPress={onMessageKeyPress}
                placeholder="Type a message here (Limit 3000 characters)..."
                className="messageTextBox"
                maxLength="3000"
                autoFocus
              />
              <InputGroup.Button>
                <Button
                  disabled={!messageText}
                  className="sendButton"
                  onClick={onSendClicked}
                >
                  Send
                  </Button>
              </InputGroup.Button>
            </InputGroup>
          </FormGroup>
        </div>
      ) : (
          <div>
            <Jumbotron>
              <h1>Hello, {props.user.name}!</h1>
              <p>Select a friend to start a chat.</p>
            </Jumbotron>
          </div>
        )}
      {showMemberModal &&
        <AddMemberModal
          show={showMemberModal}
          receivers={props.selectedChat.receivers}
          chat_type={props.selectedChat.chat_type}
          chat_list_id={props.selectedChat._id}
          setModal={_setShowMemberModal_}
          chatListData={chatListData}
          user={props.user}
          selecteList={props.selectedChat}
        />}
    </div>
  );
}


export default ChatBoxMultiUser;