import React from "react";
import { Avatar } from "react-chat-elements";

import "../style/Modal.css";

const MULTI_USER_IMAGE = 'https://p7.hiclipart.com/preview/538/485/239/multi-user-software-computer-icons-download-information-others-thumbnail.jpg';

const UserListItem = (props) => {

  return (<div className="userlist-item" onClick={() => props.onClick(props.listItem)}>
    <div className="userlist-left-section">
      {
        !props.hideImage ?
          <Avatar
            src={props.image}
            alt={"image"}
            size="large"
            type="circle flexible"
          />
          :
          <Avatar
            src={MULTI_USER_IMAGE}
            alt={"image"}
            size="large"
            type="circle flexible"
          />
      }
      <div className="userlist-name">{props.name}</div>
    </div>
    {props.showRemove &&
      <div className="userlist-name">Removed</div>
    }
    {props.showAdded && props.isAdded &&
      <div className="userlist-name">Added</div>
    }
  </div>

  );
}
export default UserListItem;