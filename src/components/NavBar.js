import React from "react";
import Navbar from "react-bootstrap/lib/Navbar";

/**
 *
 * Renders top navbar and shows the current signed in user.
 */
const NavBar = (props) => {
    return (
        <Navbar>
            <Navbar.Header>
                <Navbar.Brand onClick={() => props.changeScreen('')}>Chat App</Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                {
                    props.signedInUser && props.signedInUser.name &&
                    (<Navbar.Text pullRight>
                        Signed in as:
                    <span className="signed-in-user">
                            {props.signedInUser.name}
                        </span>
                    </Navbar.Text>)
                }

            </Navbar.Collapse>
        </Navbar>
    );
}
export default NavBar;