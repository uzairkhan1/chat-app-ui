import React from "react";
import Grid from "react-bootstrap/lib/Grid";
import Row from "react-bootstrap/lib/Row";
import Col from "react-bootstrap/lib/Col";
import List from "./List";
import ChatBox from "./ChatBox";

const UserToUser = (props) => {


    return (
        <Grid>
            <Row>
                <Col md={4}>
                    <h3> Users </h3>
                    <List
                        listData={props.usersWithPagination}
                        onClick={props.onUserChatClicked}
                        onScroll={props.onScrollUserList}
                        notFoundText={'No users to show.'}
                    />
                </Col>
                {
                    props.selectedChat._id &&
                    <Col md={8}>
                        <ChatBox
                            signedInUser={props.signedInUser}
                            onSendClicked={props.onSendClicked}
                            onBackPressed={props.onBackPressed}
                            selectedChat={props.selectedChat}
                            chat={props.chat}
                            chatWithPagination={props.chatWithPagination}
                            getUserToUserUpdatedChat={props.getUserToUserUpdatedChat}
                        />
                    </Col>
                }
            </Row>
        </Grid>
    );
}
export default UserToUser;