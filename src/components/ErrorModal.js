import React, { useState } from "react";
import Glyphicon from "react-bootstrap/lib/Glyphicon";
import Modal from "react-bootstrap/lib/Modal";
import "../style/Modal.css";

// Renders a Error modal if app encounter any error.
const ErrorModal = (props) => {
  const [show, setShow] = useState(props.show);
  return (
    <Modal show={show}>
      <Modal.Header>
        <Modal.Title>Error
        </Modal.Title>
        <i className="fa fa-close modal-close-icon" onClick={() => setShow(false)}></i>
      </Modal.Header>

      <Modal.Body>
        <h1 className="text-center">
          <Glyphicon glyph="alert" />
        </h1>
        <h5 className="text-center">{props.errorMessage}</h5>
      </Modal.Body>
    </Modal>
  );
}
export default ErrorModal;