import React, { useState, useEffect } from "react";
import Modal from "react-bootstrap/lib/Modal";
import BottomScrollListener from 'react-bottom-scroll-listener';
import { fetchChatListUsers, updateReceivers } from "../services/requests";
import {
  NotificationContainer,
  NotificationManager
} from "react-notifications";

import "../style/Modal.css";
import UserListItem from "./UserListItem";
import Helper from "../helper";

const AddMemberModal = (props) => {
  const [users, setUsers] = useState([]);
  const [pagination, setPagination] = useState({ hasNextPage: false, page: 0 });
  const [receiversId, setReceiversId] = useState([]);
  const [receivers, setReceivers] = useState([]);
  const getUsers = (query, _receiversId) => {
    fetchChatListUsers(query).then(res => {
      setPagination({ hasNextPage: res.hasNextPage, page: res.page });
      if (res.data) {
        let newUsers = [];
        newUsers = [...users, ...res.data];
        setUsers(newUsers);
      }
    }
    );
  }
  useEffect(() => {
    let _receiversIds = [];
    let _receivers = props.receivers;
    _receivers.map((item) => {
      _receiversIds.push(item._id);
      item.isAdded = true;
    });
    setReceivers(_receivers);
    setReceiversId(_receiversIds);
    setPagination({ hasNextPage: props.chatListData.hasNextPage, page: props.chatListData.page });
    setUsers(props.chatListData.data ? props.chatListData.data : []);
  }, [props.receivers, props.chatListData]);
  const onScrollEnd = () => {
    if (pagination.hasNextPage) getUsers('?chat_list_id=' + props.chat_list_id + '&page=' + (pagination.page + 1), receiversId);
  }

  const onUserClicked = (id, listName) => {
    let receiversIds = [];
    let apiMsg = '';
    let chat_type = props.chat_type == 'user-group' ? 'Group' : 'Chat';
    if (Helper.isAdmin({ _id: id }, props.selecteList.created_by)) return false;
    if (receiversId.includes(id)) {
      receiversIds = receiversId;
      receiversIds = receiversIds.filter((item) => item !== id);
      apiMsg = 'Removed from ' + chat_type + '.'
    }
    else {
      receiversIds = [...receiversId, id]
      apiMsg = 'Added in ' + chat_type + '.'
    }
    setReceiversId(receiversIds);
    let user = { userType: listName, id }
    chatListUpdateReceivers(receiversIds, apiMsg, user)

  }

  const chatListUpdateReceivers = (_receiversId, apiMsg, user) => {
    let data = {
      chat_list_id: props.chat_list_id,
      receivers: _receiversId
    }
    updateReceivers(data).then(res => {
      if (res.status === 'success') {
        NotificationManager.success("Success", apiMsg);
        if (user.userType === 'newUser') {

          let _users = users;
          let newUsers = [];
          _users.map((item) => {
            if (item._id === user.id) {
              item.isAdded = true;
              setReceivers([...receivers, item]);
            }
            else newUsers.push(item);
          })
          setUsers(newUsers);
        }
        else {
          let _receiversIds = receiversId;
          let _receivers = receivers;
          _receivers.map((item) => {
            if (item._id === user.id) {
              item.isAdded = !item.isAdded;
            }
          });
          _receiversIds.splice(_receiversIds.indexOf(user.id), 1);
          setReceivers(_receivers);
          setReceiversId(_receiversIds);
        }
      }
      else {
        NotificationManager.error(
          "Error!",
          res.error ? res.error : "Please try again!"
        );
      }
    }
    );
  }


  const renderMemberList = () => {
    return (<div className="member-container">
      <div className="member-heading">{props.chat_type == 'user-group' ? 'Group members' : 'Chat users'} </div>
      <div className="userlist-container">
        {
          receivers.length > 0 && (
            receivers.map((item, index) => {
              return <UserListItem
                index={index}
                listItem={item}
                name={item.name}
                image={item.image}
                onClick={(_item) => onUserClicked(_item._id, 'receivers')}
                showRemove={!item.isAdded}
              />

            })
          )
        }
      </div>
    </div>)
  }

  const renderUserList = () => {
    return (<>
      <div className="member-heading">{props.chat_type == 'user-group' ? 'Add Group members' : 'Chat users'} </div>
      <BottomScrollListener onBottom={onScrollEnd}>
        {scrollRef => (
          <div ref={scrollRef} className="userlist-container">
            {
              users.length > 0 && (
                users.map((item, index) => {
                  return <UserListItem
                    listItem={item}
                    name={item.name}
                    image={item.image}
                    index={index}
                    onClick={(_item) => onUserClicked(_item._id, 'newUser')}
                  />

                })
              )
            }
          </div>
        )}
      </BottomScrollListener>
    </>)
  }

  return (
    <>
      <Modal show={props.show}>
        <Modal.Header>
          <Modal.Title>Add Member
        </Modal.Title>
          <i className="fa fa-close modal-close-icon" onClick={() => props.setModal(false)}></i>
        </Modal.Header>

        <Modal.Body >
          <div>
            {renderMemberList()}
            {renderUserList()}
          </div>

        </Modal.Body>
      </Modal >
      <NotificationContainer />
    </>
  );
}
export default AddMemberModal;